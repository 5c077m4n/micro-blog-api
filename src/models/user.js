'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const hr = require('http-responder');
const to = require('await-fn');


const Schema = mongoose.Schema;
const randomNumber = (min, max) =>
	(max >= min)? Math.floor(Math.random() * (max - min + 1)) + min : min;
const wait = time => new Promise(resolve => setTimeout(resolve, time));

const UserSchema = new Schema({
	username: { type: String, required: true, unique: [
		true, 'Sorry, this username is already in use.'
	] },
	email: { type: String, trim: true },
	password: { type: String, required: true, select: false },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});
UserSchema.pre('save', async function(next) {
	const user = this;
	if(!user.password) return next();
	const [err, hash] = await to(bcrypt.hash, {
		params: [user.password, 14]
	});
	if(err) return next(err);
	user.password = hash;
	return next();
});
UserSchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error, doc));
	else next();
});

UserSchema.statics.authenticate = function(username, password, next) {
	if(!username || !password) return next(hr.unauthorized(
		'Both the username and password are required.'
	));
	else return this
		.findOne({ username })
		.select('+password')
		.exec()
		.then(async user => (user)? user :
			await wait(randomNumber(1500, 2000))
				.then(() => Promise.reject(hr.unauthorized(
					'Incorrect username/password inserted.'
				)))
		)
		.then(async user => {
			const [err, same] = await to(bcrypt.compare, {
				params: [password, user.password]
			});
			if(err) return Promise.reject(err);
			if(!same) return Promise.reject(hr.unauthorized(
				'Incorrect username/password inserted.'
			));
			user.password = undefined;
			return user;
		})
		.catch(Promise.reject);
};

module.exports = mongoose.model('User', UserSchema);
