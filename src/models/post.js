'use strict';

const mongoose = require('mongoose');
const hr = require('http-responder');


const User = mongoose.model('User');
const ObjectId = mongoose.Schema.Types.ObjectId;

const PostSchema = new mongoose.Schema({
	content: { type: String, required: true },
	userID: { type: ObjectId, required: true, ref: User },
	usersVoteUp: { type: [ObjectId], refs: User },
	usersVoteDown: { type: [ObjectId], refs: User },
	votes: { type: Number, default: 0 },
	createdAt: { type: Date, default: Date.now },
	modifiedAt: { type: Date, default: Date.now },
});
PostSchema.post('save', function(error, doc, next) {
	if(error) next(hr.internalServerError(error, doc));
	else next();
});

PostSchema.statics.getTopPosts = function() {
	return this.aggregate([
		{
			$project: {
				content: "$content",
				userID: "$userID",
				createdAt: "$createdAt",
				modifiedAt: "$modifiedAt",
				usersVoteUp: "$usersVoteUp",
				usersVoteDown: "$usersVoteDown",
				upVotes: { $size: "$usersVoteUp" },
				downVotes: { $size: "$usersVoteDown" },
			}
		}, {
			$sort: { upVotes: -1, modifiedAt: -1, downVotes: +1 }
		}
	]);
};

module.exports = mongoose.model('Post', PostSchema);
