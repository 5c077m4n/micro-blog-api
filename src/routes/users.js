'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');


const User = mongoose.model('User');
const Post = mongoose.model('Post');

router.route('/')
	.get((req, res, next) => {
		return User
			.find({})
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	});

router.route('/:userID')
	.get((req, res, next) => {
		return User
			.findById(req.params.userID)
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return User
			.findByIdAndRemove(req.params.userID)
			.exec()
			.then(data => hr.created(data).end(res))
			.catch(next);
	});

router.use(
	'/:userID/posts',
	(req, res, next) => {
		req.body.userID = req.params.userID;
		next();
	},
	require('./posts')
);

module.exports = router;
