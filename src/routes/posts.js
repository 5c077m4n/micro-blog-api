'use strict';

const router = require('express').Router();
const mongoose = require('mongoose');
const hr = require('http-responder');


const User = mongoose.model('User');
const Post = mongoose.model('Post');


router.route('/')
	.get((req, res, next) => {
		return Post
			.find({})
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.post((req, res, next) => {
		return new Post(req.body)
			.save()
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return Post
			.deleteMany({ userID: req.body.userID })
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

router.route('/new')
	.get((req, res, next) => {
		return Post
			.find({})
			.sort({ modifiedAt: -1 })
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	});

router.route('/top')
	.get((req, res, next) => {
		return Post
			.getTopPosts()
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	});

router.route('/:postID')
	.get((req, res, next) => {
		return Post
			.findById(req.params.postID)
			.exec()
			.then(data => hr.ok(data).end(res))
			.catch(next);
	})
	.put((req, res, next) => {
		return Post
			.findById(req.params.postID)
			.updateOne({}, {
				$set: {
					content: req.body.content,
					modifiedAt: Date.now()
				}
			})
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return Post
			.findByIdAndRemove(req.params.postID)
			.exec()
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

router.route('/:postID/vote-up')
	.put((req, res, next) => {
		return Post
			.findById(req.params.postID)
			.updateOne({}, {
				$addToSet: { usersVoteUp: req.body.userID },
				$pull: { usersVoteDown: req.body.userID }
			})
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return Post
			.findById(req.params.postID)
			.updateOne({}, {
				$pull: { usersVoteUp: req.body.userID }
			})
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

router.route('/:postID/vote-down')
	.put((req, res, next) => {
		return Post
			.findById(req.params.postID)
			.updateOne({}, {
				$addToSet: { usersVoteDown: req.body.userID },
				$pull: { usersVoteUp: req.body.userID }
			})
			.then(data => hr.created(data).end(res))
			.catch(next);
	})
	.delete((req, res, next) => {
		return Post
			.findById(req.params.postID)
			.updateOne({}, {
				$pull: { usersVoteDown: req.body.userID }
			})
			.then(data => hr.noContent(data).end(res))
			.catch(next);
	});

module.exports = router;
