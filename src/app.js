'use strict';

const path = require('path');

global.Promise = require('bluebird');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
const compress = require('compression');
const cors = require('cors');
const hr = require('http-responder');
/** mongoose config */
const mongoose = require('mongoose');
mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true);
require('./models'); /** Load all mongoose models before use */

const middleware = require('./middleware');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');


const dbURI = 'mongodb://mongo/micro-blog';
const app = express();

app.use(helmet());
app.options('*', cors());
app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));

mongoose.connect(dbURI, { useNewUrlParser: true })
	.then(() => console.log('[Mongoose] You have been successfully connected to the database.'))
	.catch(err => console.error(`[Mongoose] Connection error: ${err}`));
mongoose.connection
	.on('error', err => console.error(`[Mongoose] Connection error: ${err}`));

app.use(
	compress({
		filter: (req, res) => (req.headers['x-no-compression'])? false : compress.filter(req, res),
		level: 6
	})
);

app.use('/', indexRouter);
app.use('/users', middleware.verifyToken, usersRouter);

app.use((req, res, next) => next(hr.notFound('The requested page cannot be found.')));
app.use((error, req, res, next) => {
	if(!hr.isHR(error)) error = hr.improve(error);
	console.error(`[Express] ${error.stack}`);
	return error.end(res);
});

module.exports = app;
