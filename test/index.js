const crypto = require('crypto');

const assert = require('assert');
const expect = require('chai').expect;
const should = require('chai').should();
const rp = require('request-promise');
const jwt = require('jsonwebtoken');


const randomUser = 'random-testing-username';
const wait = millisec => new Promise(resolve => setTimeout(resolve, millisec));
const login = (username = randomUser, password = randomUser) => rp({
	method: 'POST',
	uri: 'http://localhost:3000/login',
	body: { username, password },
	json: true,
});
const decodeToken = token => new Promise(resolve => resolve(jwt.decode(token)));

describe('Micro-Blog API tests', function() {
	it('Should exist', async function() {
		const res = await rp({
			method: 'GET',
			uri: 'http://localhost:3000',
			json: true,
		});
		should.exist(res);
	});
	it('Should respond with "Great success!"', async function() {
		const res = await rp({
			method: 'POST',
			uri: 'http://localhost:3000/',
			json: true,
		});
		expect(res.message).equal('Great success!');
	});
	it('Should test token need for auth (no token)', async function() {
		const res = await rp({
			method: 'GET',
			uri: 'http://localhost:3000/users',
			headers: { 'x-access-token': '' },
			json: true,
		})
			.catch(res => res);
		expect(res.statusCode).equal(403);
	});
	it('Should test token need for auth (fake token)', async function() {
		const res = await rp({
			method: 'GET',
			uri: 'http://localhost:3000/users',
			headers: { 'x-access-token': 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ2c2VyIjp7Il9pZCI6IjViYTUwMjNhNmU0ZTEwMDAxZjY0MDExNCIsInVzZXJuYW1lIjoic3UiLCJlbWFpbCI6Im1haWxAbWFpbC5tYWlsIiwiY3JlYXRlZEF0IjoiMjAxOC0wOS0yMVQxNDozNzo0Ni4zNTBaIiwibW9kaWZpZWRBdCI6IjIwMTgtMDktMjFUMTQ6Mzc6NDYuMzUyWiIsIl9fdiI6MH0sImlhdCI6MTUzNzgwOTk4NCwiZXhwIjoxNTM3ODUzMTg0fQ.i9EzR_TCUMmJ4BxjaF0csAxgqPcxOWW_h3gEwHM6G2FaFZJm_FJIVb-gcA_uD5ijiQIsBAETpDInDQ_156DgNpdaZK55bHPblik-X1qikEqEV3d3sC7_zvp-jUj5aL3V_sRz1bsdXSfFYNIBTSWjR52zU2RmHopuruv0ulvGXQg7mM5YQyQmZQ9n93qw0iWQtgXGueawpKIA8_cBo54p1Z6aWRrD3qaE1sEbVmbOvNE7jCPDxuH8Z93cAyCY6dQFK8u5MYZDILifl7P7c_RtinwQRQ5iUMveD1gFVlyJYG9wNO7xzf-GSmjlqq6WlwMsImg-QQUOlER4kqT_aGm3l5paXuP6K6Bey71y42KrZB6UpYM-YrF1LlGGhj6lDRpIv4FIElv3NHPx43skNykyDozU61fBbXoPuGxdzF2M0Me1enOb-WdizXT7uxOgzT5tohXNT-MnBVUPWcd_3_XEJzl3k9mZf0JsUBxVQUJtGPWzclJzKpCyLsI9SHxbqjXpqegK0VlSf_XIF85gUBaISQiTy88fE-zhaPnygIDL2LNNs_LxdJPi5Gm_l4T_BY2cVu0uhNGnxIWJGlRdqL0k-hMBy2xrHvJkVPsiJ-lJCQz9-cZRb8Oh7iMMDjFpdw0D2cNiwTJR3x0Gu3eAb0P1yE1GJMt4_TC7veCOJTsneqw' },
			json: true,
		})
			.catch(res => res);
		expect(res.error.message).equal('invalid signature');
	});
	it('Should resister a temp user and give a new token', async function() {
		const res = await rp({
			method: 'POST',
			uri: 'http://localhost:3000/register',
			body: {
				username: randomUser,
				email: randomUser,
				password: randomUser
			},
			json: true,
		})
			.catch(error => {
				throw new Error(`Maybe the user already exsists? ${error.message}`);
			});
		expect(res.data.token).to.have.lengthOf.above(10);
	});
	it('Should login the temp user and give a new token', async function() {
		const res = await login();
		expect(res.data.token).to.have.lengthOf.above(10);
	});
	it('Should login the temp user twice and give a new tokens', async function() {
		this.timeout(15000);
		const res1 = await login();
		const res2 = await login();
		expect(res1.data.token === res2.data.token).to.equal(false);
	});
	let postsID = [];
	it('Should post 5 times', async function() {
		const userRes = await login();
		const userInfo = await decodeToken(userRes.data.token);
		let content, res;
		for(let i = 0; i < 5; i++) {
			content = crypto.randomBytes(16).toString('hex');
			res = await rp({
				method: 'POST',
				uri: `http://localhost:3000/users/${userInfo.user._id}/posts`,
				headers: { 'x-access-token': userRes.data.token },
				body: { content },
				json: true,
			});
			postsID.push(res.data._id);
		}
		expect(postsID.length).to.equal(5);
	});
	it('Should upvote the post only once (out of three attempts)', async function() {
		const userRes = await login();
		const userInfo = await decodeToken(userRes.data.token);
		const responses = [];
		for(let i = 0; i < 3; i++) {
			responses.push(await rp({
				method: 'PUT',
				uri: `http://localhost:3000/users/${userInfo.user._id}/posts/${postsID[0]}/vote-up`,
				headers: { 'x-access-token': userRes.data.token },
				json: true,
			}));
		}
		const editedRes = responses.filter(resp => resp.data.nModified);
		expect(editedRes.length).to.equal(1);
	});
	it('Should get the top posts', async function() {
		const userRes = await login();
		const userInfo = await decodeToken(userRes.data.token);
		const res = await rp({
			method: 'GET',
			uri: `http://localhost:3000/users/${userInfo.user._id}/posts/top`,
			headers: { 'x-access-token': userRes.data.token },
			json: true,
		});
		expect(
			res.data[0].upVotes >= res.data[1].upVotes &&
			res.data[1].upVotes >= res.data[2].upVotes &&
			res.data[2].upVotes >= res.data[3].upVotes
		).to.equal(true);
	});
	it('Should get the top posts 10,000 times', async function() {
		this.timeout(2 * 60 * 1000);
		const userRes = await login();
		const userInfo = await decodeToken(userRes.data.token);
		let res;
		for(let i = 0; i < 1e4; i++) {
			res = await rp({
				method: 'GET',
				uri: `http://localhost:3000/users/${userInfo.user._id}/posts/top`,
				headers: { 'x-access-token': userRes.data.token },
				json: true,
			});
		}
		expect(res.data instanceof Array).to.equal(true);
	});

	after(async function() {
		this.timeout(5000);
		const userRes = await login();
		const userInfo = await decodeToken(userRes.data.token);
		await rp({
			method: 'DELETE',
			uri: `http://localhost:3000/users/${userInfo.user._id}/posts`,
			headers: { 'x-access-token': userRes.data.token },
			json: true,
		});
		await rp({
			method: 'DELETE',
			uri: `http://localhost:3000/users/${userInfo.user._id}`,
			headers: { 'x-access-token': userRes.data.token },
			body: {
				username: '222',
				email: '222',
				password: '222'
			},
			json: true,
		});
	})
});
