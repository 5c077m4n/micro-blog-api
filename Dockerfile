FROM node:10
RUN mkdir -p /usr/src/app-blog-api/
WORKDIR /usr/src/app-blog-api/
COPY package*.json /usr/src/app-blog-api/
RUN npm install
COPY . /usr/src/app-blog-api/
EXPOSE 3000
CMD [ "npm", "start" ]
